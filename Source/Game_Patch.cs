using System;
using HarmonyLib;
using RimWorld;
using Verse;
using System.Collections.Generic;

// Adding new component props to existing defs.

namespace TechBackground
{
    [HarmonyPatch(typeof(Game), "InitNewGame")]
	public class Game_InitNewGame
	{
	    [HarmonyPrefix]
		static void Prefix()
		{
		    Game_PreInitialize.Patch_Defs();
		}
	}

    [HarmonyPatch(typeof(SavedGameLoaderNow), "LoadGameFromSaveFileNow")]
	public class Game_LoadGame
	{
	    [HarmonyPrefix]
		static void Prefix()
		{
		    Game_PreInitialize.Patch_Defs();
		}
	}


    public static class Game_PreInitialize
    {
	public static void Patch_Defs()
	{
	    ThingDef tabledef;

	    string[] table_names = {"Table1x2c", "Table2x2c", "Table2x4c", "Table3x3c"};
	    int[] max_pawns = {2, 4, 8, 12};
	    int i = 0;

	    foreach (string name in table_names)
	    {   
		tabledef = ThingDef.Named(name);
		if ((tabledef != null) && !tabledef.HasComp(typeof(Comp_StudyDesk)))
		{   
		    List<CompProperties> l = tabledef.comps;
		    l.Add(new CompProperties_StudyDesk(max_pawns[i]));

		    //Log.Message("Adding studydesk component for " + name);
		}

		i++;
	    }

	}
    }
}

