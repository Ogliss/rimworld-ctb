using System;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;
using System.Text;

// Modify floating menu item in force job, to account for multiple pawns using the same desk
// Make sure we kick only one pawn when forcing a job, instead of everyone, as per default behavior

namespace TechBackground
{
    [HarmonyPatch(typeof(FloatMenuUtility), "DecoratePrioritizedTask")]
	public class DecoratePrioritizedTask
	{
	    [HarmonyPrefix]
		static bool Prefix(FloatMenuOption option, Pawn pawn, LocalTargetInfo target, string reservedText, ref FloatMenuOption __result)
		{
			if (option.action == null) return true;
			if (!target.HasThing) return true;

			Building table = target.Thing as Building;
			if (table == null) return true;

			Comp_StudyDesk c = table.GetComp<Comp_StudyDesk>();
			if (c == null) return true;

			// Ugly, but job type if not passed here in a reasonable way
			WorkGiverDef s_def = DefDatabase<WorkGiverDef>.GetNamed("Study");
			WorkGiverDef m_def = DefDatabase<WorkGiverDef>.GetNamed("Mentor");
			if (option.Label.IndexOf(s_def.gerund) != -1)
			{
				WorkGiver_Study w = s_def.Worker as WorkGiver_Study;
				if (w.HasJobOnThing(pawn, table, true) && !w.HasJobOnThing(pawn, table, false))
				{
					Pawn p = c.students[0];
					if (p == null)
					{
						Log.Error("Force reserve study desk, but no pawns to kick out. This should not be happening.");
					}
					option.Label = option.Label + " (" + reservedText.Translate(p.LabelShort, p) + ")";

					if (p != pawn)
					{
						option.action = delegate
						{
							//Log.Message("Study, Kicking " + p);
							p.ClearReservationsForJob(p.jobs.curJob);
							Job j = w.JobOnThing(pawn, table, true);
							pawn.jobs.TryTakeOrderedJobPrioritizedWork(j, w, c.next_spot.Cell);
							p.jobs.EndCurrentOrQueuedJob(p.jobs.curJob, JobCondition.InterruptForced);
						};
					}
				}
			}
			else if (option.Label.IndexOf(m_def.gerund) != -1)
			{
				WorkGiver_Mentor w = m_def.Worker as WorkGiver_Mentor;
				if (w.HasJobOnThing(pawn, table, true) && !w.HasJobOnThing(pawn, table, false))
				{
					Pawn p;
					// Kick mentor first
					if (c.mentor != null) p = c.mentor;
					else p = c.students[0];
					if (p == null)
					{
						Log.Error("Force reserve study desk, but no pawns to kick out. This should not be happening.");
					}
					option.Label = option.Label + " (" + reservedText.Translate(p.LabelShort, p) + ")";

					if (p != pawn)
					{
						option.action = delegate
						{
							//Log.Message("Mentor, Kicking " + p);
							p.ClearReservationsForJob(p.jobs.curJob);
							Job j = w.JobOnThing(pawn, table, true);
							pawn.jobs.TryTakeOrderedJobPrioritizedWork(j, w, c.next_spot.Cell);
							p.jobs.EndCurrentOrQueuedJob(p.jobs.curJob, JobCondition.InterruptForced);
						};
					}
				}
			}

			__result = option;
			return false;
		}
	}
}
