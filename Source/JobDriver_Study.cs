using System;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;

namespace TechBackground
{
	public class JobDriver_Study : JobDriver
	{

		public static float BaseProgressPerTick = 0.0004f;
		public static float MentoringFactor = 3;

		int DefaultToilDuration = 4000;

		SkillDef relevant_skill = SkillDefOf.Intellectual;

		public static string ProgressText = "After living for an extended period of time in an advanced society, {0}'s general understanding of technology has significantly improved. [PAWN_possessive] technological background is now considered to be '{1}'.";

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			Comp_StudyDesk c = (job.targetA.Thing as ThingWithComps).GetComp<Comp_StudyDesk>();

			if (!pawn.Reserve(job.targetA, job, c.MaxPawns, 0, null, errorOnFailed)) return false;
			if (!pawn.Reserve(job.targetB, job, 1, -1, null, errorOnFailed)) return false;

			// If targetB is a chair, reserve spot as well
			if (job.targetB.HasThing)
			{
				pawn.Reserve(job.targetB.Thing.Position, job, 1, -1, null, errorOnFailed);
			}


			return true;
		}

		override protected IEnumerable<Toil> MakeNewToils()
		{
			TraitDef def = TraitDef.Named("TechBackground");
			Trait trait = pawn.story.traits.GetTrait(def);
			Comp_TechBackground_Data comp = pawn.GetComp<Comp_TechBackground_Data>();
			Comp_StudyDesk c = (this.job.targetA.Thing as ThingWithComps).GetComp<Comp_StudyDesk>();

			yield return Toil_Sit.TryToSitAtStudyDesk();
			//yield return Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.Touch);

			Toil toil = new Toil();
			toil.tickAction = delegate
			{
				float p = BaseProgressPerTick;

				if (comp.mentoring > 0)
				{
					comp.mentoring -= 1f;
					p *= MentoringFactor;
				}

				p *= (float) toil.actor.skills.GetSkill(relevant_skill).Level / 10f;

				comp.progress_today += p;
				comp.progress += p;

				toil.actor.skills.Learn(relevant_skill, SkillTuning.XpPerTickResearch, false);
			};
			toil.defaultDuration = DefaultToilDuration;
			toil.defaultCompleteMode = ToilCompleteMode.Delay;
			toil.initAction = delegate
			{
				c.students.Add(toil.actor);
			};
			toil.FailOnDestroyedNullOrForbidden(TargetIndex.A);
			toil.AddEndCondition(delegate
				{
					if (!c.Active)
					{
						return JobCondition.Incompletable;
					}
					return JobCondition.Ongoing;
				});
			toil.AddEndCondition(delegate
				{
					if ((comp.progress_today > Comp_TechBackground_Data.MaxProgressPerDay) ||
							(comp.progress > Comp_TechBackground_Data.RequiredProgress[trait.Degree]))
					{
						return JobCondition.Succeeded;
					}
					return JobCondition.Ongoing;
				});
			toil.AddFinishAction(delegate
				{
					if (comp.progress > Comp_TechBackground_Data.RequiredProgress[trait.Degree])
					{
						pawn.story.traits.allTraits.Remove(trait);
						pawn.story.traits.GainTrait(new Trait(def, trait.Degree + 1));
						comp.ResetProgress();



						Find.LetterStack.ReceiveLetter("Tech background progress",
								string.Format(ProgressText.AdjustedFor(pawn), pawn.LabelShort, pawn.story.traits.GetTrait(def).Label),
								LetterDefOf.PositiveEvent, pawn, null);
					}
					c.students.Remove(toil.actor);
				});
			yield return toil;
		}
	}

}

