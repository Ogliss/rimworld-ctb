using System;
using RimWorld;
using Verse;
using System.Collections.Generic;
using UnityEngine;

namespace TechBackground
{
	public class Comp_TechBackground_Data : ThingComp
	{
		public static float[] RequiredProgress = {1000, 3000};
		public static float MaxProgressPerDay = 20;

		public static float BaseDriftPerRareTick = 0.01f;

		public float progress = 0;
		public float progress_today = 0;
		public float mentoring = 0;

		public int progress_reset_timestamp = -1;

		public Comp_TechBackground_Data()
		{
		}

		public Comp_TechBackground_Data(Pawn p)
		{
			parent = p;
		}

		public bool FinishedToday()
		{
			return progress_today > MaxProgressPerDay;
		}

		public bool Finished()
		{
			Pawn p = parent as Pawn;
			TraitDef def = TraitDef.Named("TechBackground");
			Trait trait = p.story.traits.GetTrait(def);

			if (trait.Degree >= 2) return true;
			return progress > RequiredProgress[trait.Degree];
		}

		public void ResetProgress()
		{
			progress = 0;
			progress_today = 0;
			mentoring = 0;
		}

		public void RandomInit()
		{
			ResetProgress();
			if (parent == null) return;

			TraitDef def = TraitDef.Named("TechBackground");
			Trait trait = (parent as Pawn).story.traits.GetTrait(def);
			if (trait.Degree >= 2) return;

			progress = Rand.Range(0, RequiredProgress[trait.Degree]);
		}

		override public void PostExposeData()
		{
			base.PostExposeData();
			Scribe_Values.Look<float>(ref this.progress, "progress", 0, false);
			Scribe_Values.Look<float>(ref this.progress_today, "progress_today", 0, false);
			Scribe_Values.Look<float>(ref this.mentoring, "mentoring", 0, false);
			Scribe_Values.Look<int>(ref this.progress_reset_timestamp, "progress_reset_timestamp", -1, false);
		}

		override public void CompTickRare()
		{
			Pawn pawn = parent as Pawn;
			if (pawn.Map == null) return;

			TraitDef def = TraitDef.Named("TechBackground");
			Trait trait = pawn.story.traits.GetTrait(def);

			if (trait.Degree >= 2) return;

			float av;
			float total = 0;

			foreach (Pawn p in pawn.Map.mapPawns.FreeColonists)
			{   
				Trait tr = p.story.traits.GetTrait(def);
				total += tr.Degree;
			}

			av = total / pawn.Map.mapPawns.FreeColonistsCount;

			float pr = BaseDriftPerRareTick * (av - trait.Degree);
			if (pr > 0)
			{
				progress += pr;
				progress_today += pr;
			}

			if ((GenLocalDate.HourInteger(pawn) == 0) && ((progress_reset_timestamp < 0) || (Find.TickManager.TicksGame - progress_reset_timestamp >= 30000)))
			{
				progress_today = 0;
				progress_reset_timestamp = Find.TickManager.TicksGame;
			}

			if (progress > Comp_TechBackground_Data.RequiredProgress[trait.Degree])
			{
				pawn.story.traits.allTraits.Remove(trait);
				pawn.story.traits.GainTrait(new Trait(def, trait.Degree + 1));
				ResetProgress();



				Find.LetterStack.ReceiveLetter("Tech background progress",
						string.Format(JobDriver_Study.ProgressText.AdjustedFor(pawn), pawn.LabelShort, pawn.story.traits.GetTrait(def).Label),
						LetterDefOf.PositiveEvent, pawn, null);
			}

		}
	}
}


