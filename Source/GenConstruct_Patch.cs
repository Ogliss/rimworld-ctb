using System;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;

namespace TechBackground
{
    [HarmonyPatch(typeof(GenConstruct), "CanConstruct")]
	public class CanConstruct
	{
	    [HarmonyPostfix]
		static void Postfix(ref bool __result, Pawn p, Thing t)
		{
			TraitDef def = TraitDef.Named("TechBackground");
			if (def == null)
			{
				Log.Message("Couldn't find Trait TechBackground");
				return;
			}


			int pawn_tech_level = 0;
			bool blueprint = false;
			bool can = false;

			if (p.story.traits.HasTrait(def))
			{
				Trait tr = p.story.traits.GetTrait(def);
				pawn_tech_level = tr.Degree;
			}

			TechLevel techLevel = TechLevel.Animal;
			BuildableDef b = t.def.entityDefToBuild;

			List<ResearchProjectDef> prereq_list = b.researchPrerequisites;

			if ((t as Blueprint_Install) != null)
			{
				blueprint = true;
				prereq_list = null;
			}


			if (prereq_list != null)
			{
				for (int i = 0; i < prereq_list.Count; i++)
				{
					if (prereq_list[i].techLevel > techLevel) techLevel = prereq_list[i].techLevel;
				}
			}

			can = TechLevelMatcher.Match(pawn_tech_level, techLevel);
			// Hardcoded restrictions should not apply to blueprints
			if (!blueprint) can = can && TechLevelMatcher.Hardcoded_Restrictions(b, pawn_tech_level);
			if (!can)
			{
				JobFailReason.Is("Colonist's tech background is too low.");
			}

			__result = can && __result;
		}
	}
}
