using System;
using HarmonyLib;
using RimWorld;
using Verse;

namespace TechBackground
{

    [HarmonyPatch(typeof(PawnGenerator), "GenerateNewPawnInternal")]
	public class GenerateNewNakedPawn
	{
	    public static int MaxBackground = 3;
	    
	    public static int[] BackgroundChances_Default = {300, 390, 300, 10};
	    public static int[] BackgroundChances_Pirate = {300, 390, 300, 10};
	    public static int[] BackgroundChances_Civil = {300, 350, 300, 50};
	    public static int[] BackgroundChances_Spacer = {0, 0, 800, 200};

	    [HarmonyPostfix]
		static void Postfix(Pawn __result)
		{
			if (__result == null) return;
			TraitDef def = TraitDef.Named("TechBackground");
			if (def == null)
			{
				Log.Message("Couldn't find Trait TechBackground");
				return;
			}

			if (__result.story == null)
			{
				return;
			}

			if (__result.story.traits.HasTrait(def))
			{
				Log.Message("Pawn " + __result.Name + " already has backstory " + __result.story.traits.GetTrait(def).Degree);
			}

			if (!__result.story.traits.HasTrait(def))
			{
				AddTechBackgroundTrait(__result);
			}
		}

		public static void AddTechBackgroundTrait(Pawn p)
		{
			int deg = -1;
			TraitDef def = TraitDef.Named("TechBackground");

			// Try to guess background from the pawn's faction
			if (p.Faction != null)
			{
				deg = TechLevelMatcher.TechLevelToTechBackground(p.Faction.def.techLevel);

				// In a standard game player starts with industrial tech level, force their background to spacer
				if (p.Faction.def.defName == "PlayerColony")
				{
					deg = 2;
				}
				else if (p.Faction.def.defName == "PlayerColony-ZeroTech")
				{
					deg = 0;
				}
				else if ((p.Faction.def.fixedName == "Ancients") || (p.Faction.def.defName == "Pirate"))
				{
					deg = GetRandomBackground(p.Faction);
				}
				else if (Rand.Range(0, 100) < 5)
				{
					deg = GetRandomBackground(p.Faction);
				}

				Log.Message("Set background for " + p.Name + ": " + p.Faction.Name + ", " + p.Faction.def.techLevel + " -> " + deg);
			}

			// Adjust background based on backstory

			// Set random background, with default chances
			if (deg == -1)
			{
				deg = GetRandomBackground(null);
			}

			p.story.traits.GainTrait(new Trait(def, deg));
			Comp_TechBackground_Data c = new Comp_TechBackground_Data(p);
			c.RandomInit();
			p.AllComps.Add(c);
		}

		private static int GetRandomBackground(Faction faction)
		{
			int[] chances;
			if (faction == null) chances = BackgroundChances_Default;
			else if (faction.def.fixedName == "Ancients") chances = BackgroundChances_Spacer;
			else if (faction.def.defName == "Pirate") chances = BackgroundChances_Pirate;
			else chances = BackgroundChances_Civil;

			int deg = 0;
			int r = Rand.Range(0, 1000);
			for (int i = 0; i < MaxBackground; i++)
			{
				r -= chances[i];
				if (r > 0) deg++;
			}

			return deg;
		}


	}
}
