using System;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;

namespace TechBackground
{
    [HarmonyPatch(typeof(WorkGiver_Researcher), "HasJobOnThing")]
	public class Research_HasJobOnThing
	{
	    [HarmonyPostfix]
		static void Postfix(ref bool __result, Pawn pawn)
		{
		    if (!__result) return;
		    ResearchProjectDef proj = Find.ResearchManager.currentProj;
		    TraitDef def = TraitDef.Named("TechBackground");
		    if (def == null)
		    {
			Log.Message("Couldn't find Trait TechBackground");
			return;
		    }
		    int pawn_tech_level = 0;

		    if (pawn.story.traits.HasTrait(def))
		    {
			Trait t = pawn.story.traits.GetTrait(def);
			pawn_tech_level = t.Degree;
		    }

		    bool can = TechLevelMatcher.Match(pawn_tech_level, proj.techLevel);
		    if (!can)
		    {
			JobFailReason.Is("Colonist's tech background is too low.");
		    }

		    __result = can && __result;
		}
	}
}
