using System;
using RimWorld;
using Verse;
using System.Collections.Generic;
using UnityEngine;

namespace TechBackground
{
    public class CompProperties_StudyDesk : CompProperties
    {
	public int MaxPawns = 0;

	public CompProperties_StudyDesk()
	{
	    compClass = typeof(Comp_StudyDesk);
	}

	public CompProperties_StudyDesk(int m)
	{
	    compClass = typeof(Comp_StudyDesk);
	    MaxPawns = m;
	}
    }
}


